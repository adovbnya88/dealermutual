//
//  DMNewItemModel.swift
//  DealerMutual
//
//  Created by sasha dovbnya on 9/23/18.
//  Copyright © 2018 sasha dovbnya. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MBProgressHUD

class DMNewItemModel: DMBaseModel {
    
    func create(item: DMItem, controller: UIViewController) {
        var path = Database.database().reference().child(item.databaseKey)
        let key = item.itemID ?? path.childByAutoId().key
        path = path.child(key ?? "")
        let hud = MBProgressHUD.showAdded(to: controller.view.window ?? UIView(), animated: true)
        path.setValue(item.toJSON()) { (error, reference) in
            hud.hide(animated: true)
            if let error = error {
                controller.showError(error.localizedDescription)
            }
            else {
                controller.dismiss(animated: true, completion: nil)
            }
        }
    }
}
